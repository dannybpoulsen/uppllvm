#ifndef _TIME__
#define _TIME__

#include "model/instructions.hpp"

template<MiniMC::Model::InstructionCode>
struct TimingData{
  static int getLow () {return 1;}
  static int getHigh () {return 3;}
};

struct TData {
  int low =0;
  int high =0;
};

struct TExecutor {	

  template<::MiniMC::Model::InstructionCode op> 
  static void execute (TData& state, const ::MiniMC::Model::Instruction& i)  { 
	state.low += TimingData<op>::getLow ();
	state.high += TimingData<op>::getHigh ();
	
  }
};


#endif
