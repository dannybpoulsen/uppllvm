#include "support/casts.hpp"
namespace {
  template<::MiniMC::Model::InstructionCode e>
  struct CastExec  { 
	template<typename F,typename T>
	static void Execute (VMData& state, const ::MiniMC::Model::Instruction& i) {
	  static_assert(::MiniMC::Model::InstructionData<e>::isCast);
	  assert(i.getOpcode () == e);
	  ::MiniMC::Model::InstHelper<e> helper (i);
	  F castee = loadRegFromState<T> (state.readState,helper.getCastee ());
	  if constexpr (e == ::MiniMC::Model::InstructionCode::Trunc) {
		  if constexpr (sizeof(F) >= sizeof(T)) {
			auto res = MiniMC::Support::trunc<F,T> (castee);
			saveRegToState(res,state.writeState,helper.getResult ());
		  }
		}
		
	  else if constexpr (e == ::MiniMC::Model::InstructionCode::ZExt ||
			     e == ::MiniMC::Model::InstructionCode::BoolZExt
			     ) {
	      if constexpr (sizeof(F) <= sizeof(T)) {
		  auto res = MiniMC::Support::zext<F,T> (castee);
		  saveRegToState(res,state.writeState,helper.getResult ());
		}
	    }
	  
	  else if constexpr (e == ::MiniMC::Model::InstructionCode::SExt||
			     e == ::MiniMC::Model::InstructionCode::BoolSExt
			     ) {
	      if constexpr (sizeof(F) <= sizeof(T)) {
		  auto res = MiniMC::Support::sext<F,T> (castee);		  
		  saveRegToState(res,state.writeState,helper.getResult ());
		}
	    }
	  
	  else if constexpr (e == ::MiniMC::Model::InstructionCode::PtrToInt) {
	      
	    }
	  else if constexpr (e == ::MiniMC::Model::InstructionCode::IntToPtr) {
	      
	    }
	  
	  
	  else if constexpr (e == ::MiniMC::Model::InstructionCode::IntToBool) {
	      
	      if (castee) {
		saveRegToState<::MiniMC::uint8_t>(1,state.writeState,helper.getResult ());
	      }
	      else {
		saveRegToState<::MiniMC::uint8_t> (0,state.writeState,helper.getResult ());
	      }
	    }
	}
  };

  template<::MiniMC::Model::InstructionCode e,class F>
  struct CastExecToSelect  { 
	template<typename T>
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  return CastExec<e>::template Execute<F,T> (state,i);
	}
  };

  template<::MiniMC::Model::InstructionCode e>
  struct CastExecFromSelect  { 
	template<typename T>
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  ::MiniMC::Model::InstHelper<e> helper (i);
	  TypeRedirect<CastExecToSelect<e,T>> (state,i,helper.getResult()->getType ());
	}
  };
  
  
}
