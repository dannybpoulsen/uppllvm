#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "support/types.hpp"
#include "loaders/loader.hpp"
#include "model/modifications/simplify_cfg.hpp"
#include "model/modifications/replacememnondet.hpp"
#include "model/modifications/splitasserts.hpp"
#include "interpreter.hpp"
#include "timebound.hpp"     

extern "C" {
  MiniMC::Model::Program_ptr prgm = nullptr;
  extern const char* cir;
  const std::string ir (cir); 
  extern std::vector<std::string> entrypoints;
  extern std::size_t mem;
  
  
  struct Initialiser {
	Initialiser () {
	  MiniMC::Model::TypeFactory_ptr tfac = std::make_shared<MiniMC::Model::TypeFactory64> ();
	  MiniMC::Model::ConstantFactory_ptr cfac = std::make_shared<MiniMC::Model::ConstantFactory64> ();
	  prgm = MiniMC::Loaders::loadFromString<MiniMC::Loaders::Type::LLVM> (ir, typename MiniMC::Loaders::OptionsLoad<MiniMC::Loaders::Type::LLVM>::Opt {.tfactory = tfac,
																			      .cfactory =cfac}
	    );
	  std::unordered_map<std::string,MiniMC::Model::Function_ptr> fmap;
	  for (auto f: prgm->getFunctions ()) {
	    fmap.insert (std::make_pair(f->getName(),f));
	  }
	  for (std::string& s : entrypoints) {
	    prgm->addEntryPoint (fmap.at(s));
	  }
	  
	  MiniMC::Model::Modifications::ExpandNondet().run (*prgm);
	  MiniMC::Model::Modifications::SplitAsserts().run (*prgm);
	  if (prgm->getEntryPoints ().size() > 1) {
	    MiniMC::Model::Modifications::EnsureEdgesOnlyHasOneMemAccess().run (*prgm);
	  }
	  
	  
	}
  };
  
  static Initialiser initdummy;
	
  int nbOfFunction () {
	return prgm->getEntryPoints().size();
  }

  const char* getNameOfFunction (int f) {
	return prgm->getEntryPoints()[f]->getName().c_str();
  }
  
  int nbOfLocations (int i) {
	return prgm->getEntryPoints()[i]->getCFG ()->getLocations().size();
  }
  
  const char* getNameOfLocation (int f, int loc) {
	return prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getName().c_str();
  }
  
  int nbOfEdges (int f,int loc) {
	return  prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges().size();
  }
  
  int getToOfEdge (int f, int loc, int e) {
	auto edge = prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges ()[e].lock();
	auto to = edge->getTo ();
	int i = 0;
	for (auto l : prgm->getEntryPoints()[f]->getCFG ()->getLocations()) {
	  if (l == to.get())
		return i;
	  i++;
	}
  }

  
  
  const char* getEdgeText (int f, int loc, int e) {
	static std::string st;
	std::stringstream str;
	auto edge = prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges ()[e].lock();
	if (edge) {
	  str << *edge;
	  st = str.str();
	}
	return st.c_str();
  }

  int globalSizeState () {
	return prgm->getGlobals ()->getTotalSize();
  }
  
  int localSizeState (int f) {
	return prgm->getEntryPoints()[f]->getVariableStackDescr ()->getTotalSize();
  }

  int initGlobals (void* globalState,void* mem, int pointer, int memsize) {
	MiniMC::uint8_t* global = reinterpret_cast<MiniMC::uint8_t*> (globalState);
	MiniMC::uint8_t* local = nullptr;
	MiniMC::uint8_t* memory = reinterpret_cast<MiniMC::uint8_t*> (mem);

	auto& instr = prgm->getInitialisation ();

	auto it = instr.begin ();
	auto end = instr.end ();
	auto runVM = [&it,&end] (VMData& data) {
				   MiniMC::Util::runVM<decltype(it),VMData,Executor> (it,end,data);
				 }; 
	
	VMData state {.writeState = {.local = local, .global = global, .memory=memory, .pointer = pointer},
		      .readState = {.local = local, .global = global, .memory=memory, .pointer = pointer}
	};
	runVM (state);
	return state.writeState.pointer;
	
  }

  int evaluateGuard (void* globalState,void* localState,void* mem, int pointer, int memsize, int f, int loc, int e) {
    auto edge = prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges()[e].lock ();
    if (edge->hasAttribute<MiniMC::Model::AttributeType::Guard> ()) {
      auto& guard = edge->getAttribute<MiniMC::Model::AttributeType::Guard> ();
      MiniMC::uint8_t* global = reinterpret_cast<MiniMC::uint8_t*> (globalState);
      MiniMC::uint8_t* local = reinterpret_cast<MiniMC::uint8_t*> (localState);
      State state {.local = local, .global = global};
      auto res = loadRegFromState<MiniMC::uint8_t> (state,guard.guard);
      return guard.negate ?  !res : res;
    }
    return 1;
  }
  
  TData calcTiming (int f,int loc,int e) {
    TData data;
    auto edge = prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges()[e].lock ();
    
    if (edge->hasAttribute<MiniMC::Model::AttributeType::Instructions> ()) {
      
      auto& instr = edge->getAttribute<MiniMC::Model::AttributeType::Instructions> ();
      if (!instr.isPhi) {
	auto it = instr.begin ();
	auto end = instr.end ();
	MiniMC::Util::runVM<decltype(it),TData,TExecutor> (it,end,data);
      }
      
    }
    return data;
  }
  
  int upperBoundEdge (void* globalState,void* localState, void* mem, int pointer, int memsize, int f, int loc, int e) {
    return calcTiming (f,loc,e).high;
  }
  
  int lowerBoundEdge (void* globalState,void* localState, void* mem, int pointer, int memsize, int f, int loc, int e) {
    return calcTiming (f,loc,e).low;
  }
  
  int upperBoundLocation (void* globalState,void* localState,void* mem, int pointer, int memsize, int f, int loc) {
    int cur = 0;
    const int edges = nbOfEdges (f,loc);
    for (int i = 0;i < edges;++i) {
      cur = std::max (cur,upperBoundEdge (globalState,localState,mem,pointer,memsize,f,loc,i));
    }
    return cur;
    
  }
  
  int globalMemSize () {
    return mem;
  }
  

  
  
  void debugPrintState (int f, VMData& data) {
    auto func = prgm->getEntryPoints ()[f];
    auto stack = func->getVariableStackDescr ();
    std::cerr << "State" << std::endl;
    for (auto& v : stack->getVariables ()) {
      
      std::cerr << *v << " : ";
      
      switch (v->getType()->getSize()) {
      case 1:
	std::cerr << (uint64_t) loadRegFromState<MiniMC::uint8_t> (data.writeState, v);
	break;
      case 2:
	std::cerr <<  loadRegFromState<MiniMC::uint16_t> (data.writeState, v);
	break;
      case 4:
	std::cerr << loadRegFromState<MiniMC::uint32_t> (data.writeState, v);
	break;
      case 8:
	std::cerr << loadRegFromState<MiniMC::uint64_t> (data.writeState, v);
	break;
      }
      std::cerr << std::endl;
    }

    
    
    
  }
  
  int executeEdge (void* globalState,void* localState, void* mem, int pointer, int memsize, int f, int loc, int e) {
	
    MiniMC::uint8_t* global = reinterpret_cast<MiniMC::uint8_t*> (globalState);
    MiniMC::uint8_t* local = reinterpret_cast<MiniMC::uint8_t*> (localState);
    MiniMC::uint8_t* memory = reinterpret_cast<MiniMC::uint8_t*> (mem);
	
    auto edge = prgm->getEntryPoints()[f]->getCFG ()->getLocations()[loc]->getOutgoingEdges()[e].lock ();
    
    if (edge->hasAttribute<MiniMC::Model::AttributeType::Instructions> ()) {
      
      auto& instr = edge->getAttribute<MiniMC::Model::AttributeType::Instructions> ();
      auto it = instr.begin ();
      auto end = instr.end ();
      auto runVM = [&it,&end] (VMData& data) {
		     MiniMC::Util::runVM<decltype(it),VMData,Executor> (it,end,data);
		   };
      
      if (!instr.isPhi) {
	VMData state {.writeState = {.local = local, .global = global, .memory=memory, .pointer = pointer},
		      .readState =  {.local = local, .global = global, .memory=memory, .pointer = pointer}
	};
	runVM (state);
	return state.writeState.pointer;
      }
      else {
	std::unique_ptr<MiniMC::uint8_t[]> lglobal (new MiniMC::uint8_t[globalSizeState ()]);;
	std::unique_ptr<MiniMC::uint8_t[]> llocal (new MiniMC::uint8_t[localSizeState (f)]);
	std::unique_ptr<MiniMC::uint8_t[]> lmem (new MiniMC::uint8_t[memsize]);
	std::copy (local,local+localSizeState (f),llocal.get());
	std::copy (global,global+globalSizeState (),lglobal.get());
	std::copy (memory,memory+memsize,lmem.get());
	
	VMData state {.writeState = {.local = local, .global = global, .memory = memory, .pointer = pointer},
		      .readState = {.local = llocal.get(), .global = lglobal.get(), .memory = lmem.get(), .pointer = pointer}
	};
	runVM (state);
	
	
	
	return state.writeState.pointer;
	
      }
      
    }
    return pointer;
  }
  
}

