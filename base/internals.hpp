#include <iostream>
namespace {

  template<::MiniMC::Model::InstructionCode e>
  struct InternalExec  { 
	template<typename T>
	static void Execute (VMData& state, const ::MiniMC::Model::Instruction& i) {
	  ::MiniMC::Model::InstHelper<e> helper (i);
	  if constexpr (e == MiniMC::Model::InstructionCode::Assign) {
	      T res = loadRegFromState<T> (state.readState,helper.getValue ());
	      saveRegToState (res,state.writeState,helper.getResult ());
	      
	    }
	  else if constexpr (e == MiniMC::Model::InstructionCode::MemCpy) {
	      T size = loadRegFromState<T> (state.readState,helper.getSize ());
	      auto src = loadRegFromState<MiniMC::pointer_t> (state.readState,helper.getSource ());
	      auto trg = loadRegFromState<MiniMC::pointer_t> (state.readState,helper.getTarget ());
	      auto read = readDataFromMem (state.readState,src,size);
	      writeDataToMem (state.writeState,trg,read);
	    }
	  else {
	    std::cerr << MiniMC::Support::Localiser{"Unsupported Operation '%1%'"}.format (e) << std::endl;
	  }	
	}
  };

}
