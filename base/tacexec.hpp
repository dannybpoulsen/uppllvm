namespace {

  template<MiniMC::Model::InstructionCode e>
  struct TACExec  { 
	template<typename T>
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  static_assert(MiniMC::Model::InstructionData<e>::isTAC ||
					MiniMC::Model::InstructionData<e>::isComparison
					);
	  assert(i.getOpcode () == e);
	  MiniMC::Model::InstHelper<e> helper (i);
	  T left = loadRegFromState<T> (state.readState,helper.getLeftOp ());
	  T right = loadRegFromState<T> (state.readState,helper.getRightOp ());
	  if constexpr (e == MiniMC::Model::InstructionCode::Add) {
		  auto res = left + right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Sub) {
		  auto res = left - right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Mul) {
		  auto res = left * right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::UDiv) {
		  auto res = MiniMC::Support::div (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::SDiv) {
		  auto res = MiniMC::Support::idiv (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Shl) {
		  auto res = left << right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Shl) {
		  auto res = left << right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::LShr) {
		  auto res = MiniMC::Support::lshr (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::AShr) {
		  auto res = MiniMC::Support::ashr (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::And) {
		  auto res = left & right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Or) {
		  auto res = left | right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Xor) {
		  auto res = left ^ right;
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
		
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_SGT) {
		  auto res = MiniMC::Support::sgt (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_UGT) {
		  auto res = MiniMC::Support::ugt (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_SGE) {
		  auto res = MiniMC::Support::sgeq (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_UGE) {
		  auto res = MiniMC::Support::ugeq (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
		

	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_SLT) {
		  auto res = MiniMC::Support::slt (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_ULT) {
		  auto res = MiniMC::Support::ult (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_SLE) {
		  auto res = MiniMC::Support::sleq (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_ULE) {
		  auto res = MiniMC::Support::uleq (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_EQ) {
	      auto res = MiniMC::Support::eq (left,right);
	      saveRegToState (res,state.writeState,helper.getResult ());
	      
	    }
	  else if constexpr (e == MiniMC::Model::InstructionCode::ICMP_NEQ) {
		  auto res = MiniMC::Support::neq (left,right);
		  saveRegToState (res,state.writeState,helper.getResult ());
		}
	  else {
		std::cerr << MiniMC::Support::Localiser{"Unsupported Operation '%1%'"}.format (e) << std::endl;
	  }
	}
  };
}
