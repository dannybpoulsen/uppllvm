#include <iostream>
#include "support/pointer.hpp"

struct State {
  MiniMC::uint8_t* local;
  MiniMC::uint8_t* global;
  MiniMC::uint8_t* memory;
  int pointer;
};
extern "C" {
  int globalMemSize ();
}

struct AggregateReg {
  std::unique_ptr<MiniMC::uint8_t[]> data;
  MiniMC::uint64_t size;
};

template<class T>
T loadRegFromState (const State& state, const MiniMC::Model::Value_ptr& value) {
  if (value->isConstant ()) {
    auto cc = std::static_pointer_cast<MiniMC::Model::Constant> (value);
    return MiniMC::loadHelper<T>{cc->getData (),sizeof(T)};
  }
  else {
    assert(!value->isNonCompileConstant ());
    auto var = std::static_pointer_cast<MiniMC::Model::Variable> (value);
    MiniMC::uint8_t* readFrom = (var->isGlobal ()) ? state.global : state.local;
    return MiniMC::loadHelper<T>{readFrom + var->getPlace (),sizeof(T)};
    
  }
  
}


template<class T>
void saveRegToState (T val, const State& state, const MiniMC::Model::Value_ptr& value) {
  auto var = std::static_pointer_cast<MiniMC::Model::Variable> (value);
  MiniMC::uint8_t* saveTo = (var->isGlobal ()) ? state.global : state.local;
  MiniMC::saveHelper<T>{saveTo + var->getPlace (),sizeof(T)} = val;
}


template<class T>
T loadRegFromMemory (const State& state, MiniMC::pointer_t p) {
  MiniMC::uint8_t* readFrom = state.memory;
  return MiniMC::loadHelper<T>{readFrom + p.offset,sizeof(T)};
}
  

template<class T>
void saveRegToMemory (T val, const State& state, MiniMC::pointer_t p) {
  MiniMC::uint8_t* saveTo = state.memory;; 
  MiniMC::saveHelper<T>{saveTo + p.offset,sizeof(T)} = val;
}

struct MemRead {
  const MiniMC::uint8_t* data;
  MiniMC::uint64_t size;
};

MemRead readDataFromMem (const State& state, MiniMC::pointer_t p, MiniMC::uint64_t s){
  MiniMC::uint8_t* readFrom = state.memory;
  if (globalMemSize () >= p.offset+s) {
	MemRead {.data = readFrom + p.offset,s};
  }
  else
    std::cerr << "Accessing memory out of bounds" << std::endl;
  return MemRead {.data = readFrom,.size = 0};
}

void writeDataToMem (State& state, MiniMC::pointer_t p, MemRead data) {
  MiniMC::uint8_t* saveTo = state.memory;
  if (globalMemSize () >= p.offset+data.size) {
	std::copy(data.data,data.data+data.size,saveTo);
  }
  else {
	std::cerr << "Accessing memory out of bounds" << std::endl;
  }
}

struct VMData {
  State writeState;
  State readState;
  void finalise ()  {}
};

template<class Exec>
static void TypeRedirect (VMData& state, const ::MiniMC::Model::Instruction& i,const ::MiniMC::Model::Type_ptr& t) {
  switch (t->getTypeID ()) {
  case MiniMC::Model::TypeID::Integer: {
    switch (t->getSize ()) {
    case 1:
      return Exec::template Execute<::MiniMC::uint8_t> (state,i);
    case 2:
      return Exec::template Execute<::MiniMC::uint16_t> (state,i);
    case 4:
      return Exec::template Execute<::MiniMC::uint32_t> (state,i); 
    case 8:
      return Exec::template Execute<::MiniMC::uint64_t> (state,i); 
    }
    break;
  }
  case ::MiniMC::Model::TypeID::Bool:
    return Exec::template Execute<::MiniMC::uint8_t> (state,i);
  case ::MiniMC::Model::TypeID::Pointer:
    std::cerr << i << std::endl;	
  }
  std::cerr << "Unsupported type:"  << *t << std::endl;
  
}
