#include <type_traits>
#include "support/pointer.hpp"


namespace {

  struct MemLoader {
	template<typename T>
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  MiniMC::Model::InstHelper<MiniMC::Model::InstructionCode::Load> helper (i);
	  auto addr =  loadRegFromState<MiniMC::pointer_t> (state.writeState,helper.getAddress ());
	  T value = loadRegFromMemory<T> (state.readState,addr);
	  saveRegToState<T> (value,state.writeState,helper.getResult());
	}
  };

  struct MemSaver {
	template<typename T>
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  MiniMC::Model::InstHelper<MiniMC::Model::InstructionCode::Store> helper (i);
	  auto addr =  loadRegFromState<MiniMC::pointer_t> (state.readState,helper.getAddress ());
	  T value = loadRegFromState<T> (state.readState,helper.getValue());
	  saveRegToMemory<T> (value,state.writeState,addr);
	}
  };
  

  template<class Exec>
  static void MemTypeRedirect (VMData& state, const ::MiniMC::Model::Instruction& i,const ::MiniMC::Model::Type_ptr& t) {
	switch (t->getTypeID ()) {
	case MiniMC::Model::TypeID::Integer: {
	  switch (t->getSize ()) {
	  case 1:
		return Exec::template Execute<::MiniMC::uint8_t> (state,i);
	  case 2:
		return Exec::template Execute<::MiniMC::uint16_t> (state,i);
	  case 4:
		return Exec::template Execute<::MiniMC::uint32_t> (state,i); 
	  case 8:
		return Exec::template Execute<::MiniMC::uint64_t> (state,i); 
	  }
	  break;
	}
	case ::MiniMC::Model::TypeID::Bool:
	  return Exec::template Execute<::MiniMC::uint8_t> (state,i);
	case ::MiniMC::Model::TypeID::Pointer:
	  return Exec::template Execute<::MiniMC::pointer_t> (state,i);
	}
	std::cerr << "Unsupported type:"  << *t << std::endl;
	
  };
	
  template<MiniMC::Model::InstructionCode e>
  struct MemExec  { 
	static void Execute (VMData& state, const MiniMC::Model::Instruction& i) {
	  static_assert(MiniMC::Model::InstructionData<e>::isMemory
					);
	  assert(i.getOpcode () == e);
	  MiniMC::Model::InstHelper<e> helper (i);
	  if constexpr (e == MiniMC::Model::InstructionCode::FindSpace ||
					e == MiniMC::Model::InstructionCode::Alloca
					) {
		  auto pointer = MiniMC::Support::null_pointer ();
		  auto size =  loadRegFromState<MiniMC::uint64_t> (state.readState,helper.getSize ());
		  if (size <= state.writeState.pointer) {
		    state.writeState.pointer -= size;
		    pointer.offset = state.writeState.pointer;
		  }
		  saveRegToState<MiniMC::pointer_t> (pointer,state.writeState,helper.getResult ());
		  
	    }
	  
	  else if constexpr (e == MiniMC::Model::InstructionCode::Load) {
		  MemTypeRedirect<MemLoader> (state,i,helper.getResult()->getType ());
		}
	  else if constexpr (e == MiniMC::Model::InstructionCode::Store) {
		  MemTypeRedirect<MemSaver> (state,i,helper.getValue()->getType ());
		}
	  else {
		std::cerr << MiniMC::Support::Localiser{"Unsupported Operation '%1%'"}.format (e) << std::endl;
	  }
	}
  };
  
  
  
  
}
