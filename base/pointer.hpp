
#include "support/pointer.hpp"

namespace {

  template<::MiniMC::Model::InstructionCode e>
  struct PointerExec  { 
	template<typename T>
	static void Execute (VMData& state, const ::MiniMC::Model::Instruction& i) {
	  ::MiniMC::Model::InstHelper<e> helper (i);
	  if constexpr (e == MiniMC::Model::InstructionCode::PtrAdd) {
	      MiniMC::pointer_t address = loadRegFromState<MiniMC::pointer_t> (state.readState,helper.getAddress ());
	      T skip = loadRegFromState<T> (state.readState,helper.getSkipSize ());
	      T value = loadRegFromState<T> (state.readState,helper.getValue ());
	      auto res = MiniMC::Support::ptradd (address,skip*value);
	      
	      saveRegToState<MiniMC::pointer_t> (res,state.writeState,helper.getResult ());
	    }
	  else {
	    std::cerr << MiniMC::Support::Localiser{"Unsupported Operation '%1%'"}.format (e) << std::endl;
	  }
	}
  };
  
}
