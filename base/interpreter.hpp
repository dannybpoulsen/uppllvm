#ifndef _UPPINTERPRETER__
#define _UPPINTERPRETER__

#include "model/variables.hpp"
#include "support/localisation.hpp"
#include "support/types.hpp"
#include "support/storehelp.hpp"
#include "support/div.hpp"
#include "support/rightshifts.hpp"
#include "support/cmps.hpp"
#include "util/vm.hpp"


#include "vm.hpp"
#include "tacexec.hpp"
#include "castexec.hpp"
#include "internals.hpp"
#include "memory.hpp"
#include "pointer.hpp"

namespace {
struct Executor {	
	
	template<::MiniMC::Model::InstructionCode op> 
	static void execute (VMData& state, const ::MiniMC::Model::Instruction& i)  { 
	  ::MiniMC::Model::InstHelper<op> helper (i);
	  if constexpr (::MiniMC::Model::InstructionData<op>::isTAC || MiniMC::Model::InstructionData<op>::isComparison) {
	      TypeRedirect<TACExec<op>> (state,i,helper.getLeftOp()->getType ());
	    }
	  else if constexpr (::MiniMC::Model::InstructionData<op>::isCast) {
	      TypeRedirect<CastExecFromSelect<op>> (state,i,helper.getCastee()->getType ());
	    }
	  
	  else if constexpr (::MiniMC::Model::InstructionData<op>::isMemory) {
	      ::MemExec<op>::Execute (state,i);
	    }
	  
	  else if constexpr (op == MiniMC::Model::InstructionCode::Assign) {
	      TypeRedirect<InternalExec<op>> (state,i,helper.getValue()->getType());
	    }
	  else if constexpr (op == MiniMC::Model::InstructionCode::MemCpy) {
	      TypeRedirect<InternalExec<op>> (state,i,helper.getSize()->getType());
	    }
	  else if constexpr (::MiniMC::Model::InstructionData<op>::isPointer) {
	      TypeRedirect<PointerExec<op>> (state,i,helper.getValue()->getType());
	    }
	  else {
	    std::cerr << MiniMC::Support::Localiser{"Unsupported Operation '%1%'"}.format (op) << std::endl;
	  }
	}
  
	
	
  };

}
#endif
