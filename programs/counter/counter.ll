@c = common dso_local global i32 0, align 4

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @add() #0 {
  br label %1

1:                                                ; preds = %0, %1
  %2 = load i32, i32* @c, align 4
  %3 = add nsw i32 %2, 1
  store i32 %3, i32* @c, align 4
  br label %1
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @sub() #0 {
  br label %1

1:                                                ; preds = %0, %1
  %2 = load i32, i32* @c, align 4
  %3 = add nsw i32 %2, -1
  store i32 %3, i32* @c, align 4
  br label %1
}

define dso_local void @check() #0 {
  br label %1

1:                      
  %2 = load i32, i32* @c
  %3 = icmp sgt i32 %2, 5
  br i1 %3, label %end, label %1
end: 
 ret void
}