void assert (int);
char read ();

#define N 6

int main () {
  char sec[N];
  for (int i = 0; i <N; i++) {
    sec[i] = 'a'+i;
  }
  for (int i = 0; i< N; i++) {
    char k = read();
    if (k != sec[i])
      assert(0);
  }
  return 0;
}
