import os
import llvmgendyn
import sys

dir = os.path.dirname (os.path.abspath (__file__))

prgms = [
    (os.path.join (dir,"simpel.ll"),["main"],50),
    (os.path.join (dir,"nd_unfold.ll"),["main"],50),
    (os.path.join (dir,"petersons.ll"),["petersons1","petersons2"],200),
    (os.path.join (dir,"petersonsBuggy.ll"),["petersons1","petersons2"],200),
    (os.path.join (dir,"test.ll"),["main"],200),
    (os.path.join (dir,"sec.ll"),["main"],200),
    #(os.path.join (dir,"loop.ll"),["main"],200),
    #(os.path.join (dir,"phi.ll"),["main"],200)
    
]


sys.stdout.write(";".join(llvmgendyn.generateSeveralToDir (sys.argv[1],prgms)))



