; ModuleID = 'petersonsBuggy.c'
source_filename = "petersonsBuggy.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.Options = type { i32*, i32*, i32* }

@turn = dso_local global i32 0, align 4
@crit1 = dso_local global i32 0, align 4
@crit2 = dso_local global i32 0, align 4
@oneflag = common dso_local global i32 0, align 4
@secondflag = common dso_local global i32 0, align 4

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @crit() #0 {
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @petersons1() #0 {
  %1 = alloca %struct.Options, align 8
  %2 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  store i32* @oneflag, i32** %2, align 8
  %3 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 1
  store i32* @secondflag, i32** %3, align 8
  %4 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  store i32* @turn, i32** %4, align 8
  %5 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  %6 = load i32*, i32** %5, align 8
  store i32 0, i32* %6, align 4
  %7 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  %8 = load i32*, i32** %7, align 8
  store i32 1, i32* %8, align 4
  br label %9

9:                                                ; preds = %21, %0
  %10 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 1
  %11 = load i32*, i32** %10, align 8
  %12 = load i32, i32* %11, align 4
  %13 = icmp ne i32 %12, 0
  br i1 %13, label %14, label %19

14:                                               ; preds = %9
  %15 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  %16 = load i32*, i32** %15, align 8
  %17 = load i32, i32* %16, align 4
  %18 = icmp eq i32 %17, 1
  br label %19

19:                                               ; preds = %14, %9
  %20 = phi i1 [ false, %9 ], [ %18, %14 ]
  br i1 %20, label %21, label %22

21:                                               ; preds = %19
  br label %9

22:                                               ; preds = %19
  store i32 1, i32* @crit1, align 4
  store i32 0, i32* @crit1, align 4
  %23 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  %24 = load i32*, i32** %23, align 8
  store i32 0, i32* %24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @petersons2() #0 {
  %1 = alloca %struct.Options, align 8
  %2 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  store i32* @secondflag, i32** %2, align 8
  %3 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 1
  store i32* @oneflag, i32** %3, align 8
  %4 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  store i32* @turn, i32** %4, align 8
  %5 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  %6 = load i32*, i32** %5, align 8
  store i32 1, i32* %6, align 4
  %7 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  %8 = load i32*, i32** %7, align 8
  store i32 0, i32* %8, align 4
  br label %9

9:                                                ; preds = %21, %0
  %10 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 1
  %11 = load i32*, i32** %10, align 8
  %12 = load i32, i32* %11, align 4
  %13 = icmp ne i32 %12, 0
  br i1 %13, label %14, label %19

14:                                               ; preds = %9
  %15 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 2
  %16 = load i32*, i32** %15, align 8
  %17 = load i32, i32* %16, align 4
  %18 = icmp eq i32 %17, 0
  br label %19

19:                                               ; preds = %14, %9
  %20 = phi i1 [ false, %9 ], [ %18, %14 ]
  br i1 %20, label %21, label %22

21:                                               ; preds = %19
  br label %9

22:                                               ; preds = %19
  store i32 1, i32* @crit2, align 4
  store i32 0, i32* @crit2, align 4
  %23 = getelementptr inbounds %struct.Options, %struct.Options* %1, i32 0, i32 0
  %24 = load i32*, i32** %23, align 8
  store i32 0, i32* %24, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{!"clang version 10.0.0 "}
