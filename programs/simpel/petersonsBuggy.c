
#define ARR

void crit () {}

typedef struct {
  int *mflag;
  int *oflag;
  int* turn;
}Options;

int turn = 0;
int oneflag;
int secondflag;

int crit1 = 0;
int crit2 = 0;


void petersons1 () {  
  Options opt;
  opt.mflag = &oneflag;
  opt.oflag = &secondflag;
  opt.turn = &turn;
  
  *opt.mflag = 0;
  *opt.turn = 1;
  
  while (*opt.oflag  && *opt.turn == 1)
	{
	  // busy wait
	}
  // critical section
  crit1 = 1;
  // end of critical section
  crit1 = 0;
  *opt.mflag = 0;
}

void petersons2 () {
  Options opt;
  opt.mflag = &secondflag;
  opt.oflag = &oneflag;
  opt.turn = &turn;
  
  *opt.mflag = 1;
  *opt.turn = 0;
  
  while (*opt.oflag  && *opt.turn == 0)
	{
	  // busy wait
	}
  // critical section
  crit2 = 1;
  // end of critical section
  crit2 = 0;
  *opt.mflag = 0;
}
