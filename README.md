# Analysis of Source Code Using Uppaal  
This repository contains supplemental code for the paper "Analysis of
Source Code Using Uppaal". The repository uses a combination of cmake
and python to generate a Uppaal model for an llvm. The generated model
can be used for verifying properties of the llvm-program such as
mutual exclusion. 

## Repository Structure  
├── base - base code for creating dynamic link library  
├── CMakeLists.txt  
├── external  
│   ├── CMakeLists.txt  
│   └── minimc  
├── programs - contains a few example programs  
└── python  
    ├── external  
    │   └── pyuppaal  - used for generating uppaal models (mirrore from  https://launchpad.net/pyuppaal with small modifications)  
    ├── llvmgendyn.py - generates a C-library file that can be compiled into dynamic link library  
	├── llvmtouppaal.py - generate uppaal model based on the dynamic link library  


## Setting up for compilation  
After cloning this repository you need to:
1. Run: `git submodule update --init --recursive` to initialise all
   external repositories used (minimc, and murmurhash and their
   dependencies)  

2. Run `mkdir build` to create a separate build directory  

3. Run `cd build`

4. Run `cmake .. ` to setup a build system in your build
   directory 

5. Run `make` to run the compilation process. 

After step 4) Uppaal files should have been generated in
`build/programs/simpel/`. These can be interacted using [Uppaal-Stratego](https://people.cs.aau.dk/~marius/stratego/).

## Programs/models used in the paper  

After compilation the models used for the paper are : 
- `build//programs/simpel/sec.xml` 

## Recreating Figure 4: 
Recreating Figure 4 of the paper requires 

- adding a global clock `G` to the generated Uppaal-model. For doing this add a statement `clock G;`
to the global declarations section.
- Execute Query: `E[<=500;1000] (max: G*(1-main.AssertViolation || main.main_Term))` in the verification tab. Notice that there is an query writting in the paper draft. 

