import pyuppaal.pyuppaal as pyuppaal
import math 
def makeTemplateForFunction (lib,fid,locS):
    locs = []
    edges = []

    nbLocations = lib.nbOfLocations (fid)
    for i in range(0,nbLocations):
        lib.getNameOfLocation.restype = c_char_p
        lib.getEdgeText.restype = c_char_p
        invariant = ""
        if lib.nbOfEdges (fid,i) >= 1:
            invariant = "x<=upperBoundLocation (glob,lstate,memory,pointer,memsize,{0},{1})".format(fid,i)
        locs.append(pyuppaal.Location (name = lib.getNameOfLocation (fid,i).decode('ascii').replace (".","_").replace (":","_"),invariant= invariant))
    
    for i,loc in enumerate (locs):
        for e in range (0,lib.nbOfEdges (fid,i)):
            
            edges.append(pyuppaal.Transition (source = loc,
                                              target = locs[lib.getToOfEdge (fid,i,e)],
                                              assignment="pointer = executeEdge (glob,lstate,memory,pointer,memsize,{0},{1},{2}),x=0".format (fid,i,e),
                                              guard = "evaluateGuard (glob,lstate,memory,pointer,memsize,{0},{1},{2}) && x>=lowerBoundEdge (glob,lstate,memory,pointer,memsize,{0},{1},{2}) && x<=upperBoundEdge (glob,lstate,memory,pointer,memsize,{0},{1},{2}) ".format (fid,i,e),
                                              comment = lib.getEdgeText (fid,i,e).decode('ascii')
            )
                         )
            
        
    return pyuppaal.Template (lib.getNameOfFunction (fid).decode('ascii').replace (".","_").replace (":","_"), locations = locs,initlocation = locs[0],transitions =edges, declaration = "mem_t lstate[{0}];clock x;".format(locS))

def makeInitGLobals ():
    locs = [pyuppaal.Location (name ="Init", committed = True),
            pyuppaal.Location (name ="Done")]

    edges = [pyuppaal.Transition(source = locs[0], target =locs[1], assignment = "pointer = initGlobals (glob,memory,pointer,memsize)")]
    return pyuppaal.Template (name = "glob_init",locations = locs,transitions = edges,initlocation = locs[0])

def run (libpath,output):
    lib = cdll.LoadLibrary (os.path.abspath (sys.argv[1]))
    # lib.initialise ()
    templates = []
    nbOfFunctions = lib.nbOfFunction ()
    memsize = lib.globalMemSize () 
    lib.getNameOfFunction.restype = c_char_p
    
    locS =  int((max([lib.localSizeState(i) for i in range (0,nbOfFunctions)]))/4+1) #int(math.ceil (max([1]+[1+lib.localSizeState (i) / 8 for i in range(0,nbOfFunctions) ])))
    globS = int((lib.globalSizeState ()/4+1)) #int(math.ceil(1+lib.globalSizeState() / 8))
    mem = int((memsize/4+1)) #int(math.ceil(1+memsize / 8))
    fnames = [lib.getNameOfFunction (i).decode('ascii')  for i in range (0,nbOfFunctions)]
    for f in range(0,nbOfFunctions):
        templates.append (makeTemplateForFunction (lib,f,locS))
    templates.append (makeInitGLobals ())
        
    for t in templates:
        t.assign_ids ()
        t.layout ()
    
             
    decl = '''
    typedef int[-(1<<31),(1<<31)-1] int32_t;
    
/*    typedef struct {{
      int32_t a;
      int32_t b;
    }}mem_t;*/
    typedef int32_t mem_t;
    
    mem_t glob[{1}];
    mem_t memory[{3}];
    const int memsize = {4};
    int pointer = {4}-1;

    import "{0}" {{
    int executeEdge (mem_t& glob[{1}], mem_t& loc[{2}], mem_t& loc[{3}], int p,int s,int f, int l, int e);
    int lowerBoundEdge (const mem_t& glob[{1}], const mem_t& loc[{2}], const mem_t& loc[{3}], int p,int s,int f, int l, int e);
    int upperBoundEdge (const mem_t& glob[{1}], const mem_t& loc[{2}],const mem_t& loc[{3}], int p,int s,int f, int l, int e);
    int upperBoundLocation (const mem_t& glob[{1}], const mem_t& loc[{2}],const mem_t& loc[{3}], int p,int s,int f, int l);
    int evaluateGuard (const mem_t& glob[{1}], const mem_t& loc[{2}],const mem_t& loc[{3}], int p,int s,int f, int l, int e);
    int initGlobals (const mem_t& glob[{1}], const mem_t& loc[{3}], int p, int memsize);
    
}};'''.format (os.path.abspath (sys.argv[1]),globS,locS,mem,memsize)    
    nta = pyuppaal.NTA (templates = templates,declaration = decl, system = "system {0};".format (",".join (fnames+["glob_init"])))
    with open(output,'w') as writeto:
        writeto.write(nta.to_xml ())
    


if __name__ == "__main__":
    from ctypes import *

    import sys
    import os
    assert(len(sys.argv) == 3)
    libpath = sys.argv[1]
    out = sys.argv[2]
    
    run (libpath,out)
