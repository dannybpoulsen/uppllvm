def genCDynamicLink(llpath,entries,mem,outp):    

    with open(llpath,'r') as inp, open(outp,'w') as outf:
        llvm = inp.read()
        out = '''
        #include <vector>
        #include <string>
        extern "C" {{
        const char* cir = R"( {0}
        )";
        
        std::vector<std::string> entrypoints {{{1} }};
        std::size_t mem = {2};
        }}
        '''.format (llvm,",".join (['''"{0}"'''.format(e) for e  in entries]),mem)
        outf.write (out)
         
    

def generateSeveralToDir (outdir,inputs):
    import os
    generated = []
    for inppath,entries,mem in inputs:
        g = os.path.join (outdir,os.path.basename(inppath).replace(".ll",".cpp"))
        genCDynamicLink (inppath,entries,mem,g)
        generated.append(g)
    return [os.path.basename(f).split(".")[0] for f in generated]


        
if __name__ == "__main__":
    import sys

    assert(len (sys.argv) > 2)

    llpath  = sys.argv[1]
    outpath = sys.argv[2]
    entries = sys.argv[3:]
    genCDynamicLink (llpath,entries,outpath)
